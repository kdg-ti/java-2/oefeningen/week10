package be.kdg.threads;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExecuteFactoren {
    private static final long DEMOGETAL = 214_577_422_401L;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Geef een getal: ");
        long getal = DEMOGETAL;
        try {
            getal = scanner.nextLong();
        } catch (InputMismatchException e) {
            // foutieve invoer, we nemen het demogetal
            getal = DEMOGETAL;
        }

        //TODO Hier aanvullen

        if (FactorenRunnable.getAantalFactoren() == 0) {
            System.out.println("Dit is een priemgetal!");
        }
    }
}

/*
Invoer van 77.422.401 geeft:

Factor      3 --> Thread-0
Factor    147 --> Thread-1
Factor    419 --> Thread-4
Factor   1257 --> Thread-12
Factor      7 --> Thread-0
Factor   3771 --> Thread-37
Factor   2933 --> Thread-29
Factor      9 --> Thread-0
Factor    441 --> Thread-4
Factor     21 --> Thread-0
Factor     49 --> Thread-0
Factor     63 --> Thread-0
Factor   8799 --> Thread-87
*/

/*
Invoer van 4.577.422.401 geeft:

Factor      3 --> Thread-0
Factor      9 --> Thread-0
Factor     27 --> Thread-0
*/

/*
Invoer van 14.577.422.401 geeft:

Dit is een priemgetal!
*/

/*
Invoer van 214.577.422.401 geeft:

Factor    181 --> Thread-1
Factor    543 --> Thread-5
Factor      3 --> Thread-0
Factor    829 --> Thread-8
Factor   2487 --> Thread-24
Factor 150049 --> Thread-1500
Factor 450147 --> Thread-4501
*/